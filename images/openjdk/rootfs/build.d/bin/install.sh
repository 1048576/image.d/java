#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "microdnf install findutils"

# .post
_sh "rm -rf /build.d/"
