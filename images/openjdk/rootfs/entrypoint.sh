#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

for script in $(find /entrypoint.d/bin/ -type f); do
    _sh "${script}"
done

exec java ${JAVA_PROXY_OPTIONS} ${JAVA_KEYSTORE_OPTIONS} $@
